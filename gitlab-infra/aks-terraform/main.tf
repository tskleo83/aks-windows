terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "~> 3.54.0"
    }
    random = {
      source = "hashicorp/random"
      version = "~> 3.5.1"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "rg" {
  name     = var.resource_group
  location = var.location
}

resource "azurerm_virtual_network" "vnet" {
  name                = "virtualNetwork1"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  address_space       = ["10.1.0.0/16"]

  subnet {
    name           = "subnet1"
    address_prefix = "10.1.0.0/16"
  }
}

resource "random_password" "winnode" {
  length = 16
}

resource "azurerm_kubernetes_cluster" "aks" {
  count               = try(var.disable_create_cluster ? 0 : 1, 1)   
  name                = "glaks1"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  dns_prefix          = "aks1"

  default_node_pool {
    name           = "lin"
    node_count     = var.node_count_linux
    vm_size        = var.windows_node_vmtype
    vnet_subnet_id = element(tolist(azurerm_virtual_network.vnet.subnet),0).id
  }

  windows_profile {
    admin_username = "azadmin"
    admin_password = random_password.winnode.result
  }
  
  network_profile {
    network_plugin = "azure"
  }

  identity {
    type = "SystemAssigned"
  }
}

resource "azurerm_kubernetes_cluster_node_pool" "win" {
  count               = try(var.disable_create_cluster ? 0 : 1, 1) 
  name                  = "win"
  kubernetes_cluster_id = azurerm_kubernetes_cluster.aks[0].id
  vm_size               = "standard_ds3_v2"         # Must have 128GB of temp disk to be an AKS node
  node_count            = var.node_count_windows
  os_type               = "Windows"
  os_sku                = var.azure_tf_provider_windows_os_sku
  node_taints           = ["os=windows:NoSchedule"] # Force non-scheduling to prevent non-os specified helm installs from hitting Windows
}