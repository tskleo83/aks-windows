## Debugging and Troubleshooting Instance Runner

One of the most challenging aspects of configuring nodes for no human access is debugging while developing the automation that deploys these nodes. Generally it is also harder in all clouds to debug on-instance automation that is kicked off by the cloud agent rather than through more traditional connection methods. However, the benefit of using the cloud agent is that no additional connectivity APIs or firewall ports need to be opened and then returned to a pristine state when done. The following information helps with troubleshooting problems with your runner node provisioning automation.

The template and the following instructions assume you are going to use the azure client to proxy ssh so that port 22 does not need to be opened.

## Overview

There are several best practices in the instance script that help it to be easier to debug and easier to call:

- Beware that some syntax failures in the powershell executable command line appears to be eaten and not surfaced anywhere, hence the next bullet.
- Boolean parameters are typed as strings and cast to boolean in the script, this avoids problems trying to pass the powershell boolean value that is preceeded with the dollar ($) character via other languages (e.g. terraform/bash) and operating systems (e.g. Azure CloudShell Bash). They can be passed as: 0/1 (as number or string), 'true'/'false' (in any character case)
- Start-Transcript is used to create a log of script activities.

### Key Files and Directories

- **C:\WindowsAzure** - Windows Agent binaries, configuration and logging.
  - **c:\WindowsAzure\Logs** - all logs, some in text (*.log), some as windows event export files (*.evl) - query commands below
  - **c:\WindowsAzure\Logs\waappagent.log** - generally 95% of relevant messages are here.
  - **C:\WindowsAzure\Logs\Plugins\Microsoft.Compute.CustomScriptExtension\1.9.5** - logs specific to extension.
    - `cat C:\WindowsAzure\Logs\Plugins\Microsoft.Compute.CustomScriptExtension\*\CustomScriptHandler.log` - detailed log.
- **C:\Packages\Plugins\Microsoft.Compute.CustomScriptExtension\1.9.5** - (version directory will change over time) Custom Script Extension binaries, configuration and logging.
  - `cat C:\Packages\Plugins\Microsoft.Compute.CustomScriptExtension\*\Status\0.status` - possible global status
  - **C:\Packages\Plugins\Microsoft.Compute.CustomScriptExtension\1.9.5** - utility CMDs for managing agent - could possibly be called by `az vm run-command`
  - **C:\Packages\Plugins\Microsoft.Compute.CustomScriptExtension\1.9.5\Downloads\0\SCRIPTNAME.ps1** - rendered script - call to test.
    - `ls C:\Packages\Plugins\Microsoft.Compute.CustomScriptExtension\1.9.5\Downloads\0\shell-runner-with-docker-install.ps1` - did script copy down. Can rerun it to find some kinds of errors.
- **Windows Event Log** - Log: 'Application', Log Provider: 'WindowsAzureGuestAgent' (querying commands given below)

### Retrieving Provisioning Errors without External Access

If you did not configure external access, there may be enough information in the following file to diagnose some (if the script exited without hanging): 
Note: `*` in the below path is instead of the version of the extension, which was '1.9.5' as of this writing.
`az vm run-command invoke -g gitlab-aks-windows-rg-2  -n GL-SHELL-RUNNER  --command-id RunPowerShellScript --scripts "cat C:\Packages\Plugins\Microsoft.Compute.CustomScriptExtension\*\Status\0.status"`

Get script log to see how far it got:
az vm run-command invoke -g gitlab-aks-windows-rg-2  -n GL-SHELL-RUNNER  --command-id RunPowerShellScript --scripts "cat C:/Users/Public/azurerm_virtual_machine_extension/initialprovisioning.log"`

### Manual Remote SSH Setup

If the on-instance provisioning script completely fails before ssh is installed or you need a quick SSH install to access the node for any reason. An external IP address and security rule is also needed - which will already exist if you have enabled the tf variable external_access. This first command enable ssh will not work until the terraform CustomScriptExtension command fails or after the 30 minute timeout.

1. `az vm run-command invoke -g gitlab-aks-windows-rg-2  -n GL-SHELL-RUNNER  --command-id RunPowerShellScript --scripts "Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0;Start-Service sshd;Set-Service -Name sshd -StartupType 'Automatic'"`
2. `az ssh vm -g gitlab-aks-windows-rg-2 -n GL-SHELL-RUNNER --local-user gitlab-runner`

The following code to start the service and openning ports on the **local firewall** is **NOT** needed (port stays closed) if you use `az ssh vm` instead of the normal `ssh` client. az ssh tunnels the request and turns it over to port 22 locally on the vm. The normal ssh client will require the following to run on the instance - it can be added to the above openssh install command.

"New-NetFirewallRule -Name 'OpenSSH-Server-In-TCP' -DisplayName 'OpenSSH Server (sshd)' -Enabled True -Direction Inbound -Protocol TCP -Action Allow -LocalPort 22"

### SSH Configuration

The closest thing to zero external ports to support shell access on Azure is setting up access to the VM with NSG rules that only allow access from Azure CloudShell. A public IP address is required, but it will only respond to the ports from Azure CloudShell. Since runner shell access should only be necessary to debug changes to runner provisioning scripts, this automation does not implement a bastion host on a public subnet - it uses a simpler implementation of creating and assigning a public IP directly to the runner host.
[Tutorial on NSG Rule Restricting Access to CloudShell](https://techcommunity.microsoft.com/t5/itops-talk-blog/cloud-shell-quick-tip-service-tag-network-security-group-rule/ba-p/2993204)

Terraform Variable: external_access = true
- Adds a public IP to the runner vm
- Adds a Network Security Group Rule to only enable SSH from Azure CloudShell.
- Installs and activates SSH Server on instance if external_access is on

The above items make failure debugging much easier as they are configured even if the on-instance script fails.

### Disabling Cluster Creation

Repetitive testing of challenges with the runner instance setup are slowed down the the creation and deletion of the AKS cluster. To make these cycles faster, set the Terraform variable "disable_create_cluster" to "true".

### PowerShell Commands For Debugging azurerm_virtual_machine_extension via SSH

SSH starts with CMD shell, so execute "powershell" before attempting these commands:

#Install chocolatey and nano package (any other package is possible) for viewing logs and editing scripts in ssh console
If (!(Test-Path env:chocolateyinstall)) {iwr https://chocolatey.org/install.ps1 -UseBasicParsing | iex} ; choco install -y nano

#Event log - not very helpful, only numbers
get-winevent -ProviderName WindowsAzureGuestAgent | format-list

#From: https://learn.microsoft.com/en-us/troubleshoot/azure/virtual-machines/windows-azure-guest-agent
ls -force c:\WindowsAzure\Logs

#Overall status of terraform initiated job - will show a final status IF script did not hang (exited)
cat C:\Packages\Plugins\Microsoft.Compute.CustomScriptExtension\1.9.5\Status\0.status

#Grep all logs
select-string -path c:\WindowsAzure\Logs\*.log -SimpleMatch "CustomScriptExtension"

#Grep most relevant log
select-string -path c:\WindowsAzure\Logs\waappagent.log -SimpleMatch "CustomScriptExtension"

get-winevent -path *.etl -Oldest | where LevelDisplayName -ne "Information" | Sort-Object TimeCreated
