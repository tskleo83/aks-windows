
#Creates Windows Instance with Docker and GitLab Shell Runner for Docker Container Building
#Reuses the vnet and subnet created in main.tf

resource "azurerm_network_interface" "gl_shell_runner_nic" {
  name                = "gl_shell_runner_nic"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  ip_configuration {
    name                          = "gl_shell_runner_nic"
    #subnet_id                     = azurerm_subnet.gmsasubnet.id
    subnet_id                     = element(tolist(azurerm_virtual_network.vnet.subnet),0).id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = try(var.external_access ?  azurerm_public_ip.glrunner_ip[0].id : null, null) 
  }
}

resource "azurerm_windows_virtual_machine" "gl_shell_runner" {
  name                = "GL-SHELL-RUNNER"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  size                = "Standard_D4s_v3"
  admin_username      = var.win_username
  admin_password      = var.win_userpass
  network_interface_ids = [
    azurerm_network_interface.gl_shell_runner_nic.id
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = var.azurerm_windows_virtual_machine_sku
    version   = "latest"
  }
}

# **************
# The following creates a public IP address on the NIC 
# That can only be access from Azure CloudShell
# It is only intended for troubleshooting while developing
# The runner node automation and should not be enabled for
# normal usage.
# It is triggered by setting the tf variable external_access = true

#Optional: Creates a public IP address for the Runner VM
resource "azurerm_public_ip" "glrunner_ip" {
  count               =  try(var.external_access ? 1 : 0, 0) 
  name                = "glrunner_ip"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method   = "Static"
  sku                 = "Standard"
}

# * * * * * * *  NSG / Security rule for Runner to allow only SSH/RDP traffic from CloudShell * * * * * * *
resource "azurerm_network_security_group" "runner-access-nsg" {
  count               = try(var.external_access ? 1 : 0, 0) 
  name                = "runner-access-nsg"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  security_rule {
    # Ingress traffic from CloudShell on 22 is enabled
    name                       = "AllowIB_SSH_FromCloudShell"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = ["22","5985"]
    source_address_prefix      = "AzureCloud"
    destination_address_prefix = "*"
  }
}

# Associate the NSG to the LinuxVM's NIC
resource "azurerm_network_interface_security_group_association" "glrunner-nic-and-nsg-association" {
  count                     = try(var.external_access ? 1 : 0, 0) 
  network_interface_id      = azurerm_network_interface.gl_shell_runner_nic.id
  network_security_group_id = azurerm_network_security_group.runner-access-nsg[0].id
}

# **************
# script parameter cannot exceed 8191 characters (https://docs.microsoft.com/en-us/troubleshoot/windows-client/shell-experience/command-line-string-limitation#more-information)
#Provision software for shell runner
resource "azurerm_virtual_machine_extension" "install_glrunner" {
  name                 = "install_glrunner"
  virtual_machine_id   = azurerm_windows_virtual_machine.gl_shell_runner.id
  publisher            = "Microsoft.Compute"
  type                 = "CustomScriptExtension"
  type_handler_version = "1.9"

  protected_settings = <<SETTINGS
  {    
    "timestamp": "formatdate(\"YYYYMMDDhhmmss\", timestamp)",
    "autoUpgradeMinorVersion": true,
    "commandToExecute": "powershell -command \"[System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String('${base64encode(data.template_file.shell-runner-with-docker-install.rendered)}')) | Out-File -encoding utf8 -filepath shell-runner-with-docker-install.ps1\" && powershell -ExecutionPolicy Unrestricted -File shell-runner-with-docker-install.ps1 -GitLabURL ${data.template_file.shell-runner-with-docker-install.vars.GitLabURL} -GitLabRegistryURI ${data.template_file.shell-runner-with-docker-install.vars.GitLabRegistryURI} -GitLabRunnerTokenList ${data.template_file.shell-runner-with-docker-install.vars.GitLabRunnerTokenList} -external_access ${data.template_file.shell-runner-with-docker-install.vars.external_access}"
  }
  SETTINGS
}

#latest gzipped  "commandToExecute": "powershell -command \"$is=[IO.MemoryStream]::New([System.Convert]::FromBase64String('${base64gzip(data.template_file.shell-runner-with-docker-install.rendered)}')); $gs=[IO.Compression.GzipStream]::New($is, [IO.Compression.CompressionMode]::Decompress); $r=[IO.StreamReader]::New($gs, [System.Text.Encoding]::UTF8); Set-Content -Path shell-runner-with-docker-install.ps1 -Stream $($r.ReadToEnd()); $r.Close();\" && powershell -ExecutionPolicy Unrestricted -File shell-runner-with-docker-install.ps1 -GitLabURL ${data.template_file.shell-runner-with-docker-install.vars.GitLabURL} -GitLabRunnerTokenList ${data.template_file.shell-runner-with-docker-install.vars.GitLabRunnerTokenList} -external_access ${data.template_file.shell-runner-with-docker-install.vars.external_access}"
#working "commandToExecute": "powershell -command \"[System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String('${base64encode(data.template_file.shell-runner-with-docker-install.rendered)}')) | Out-File -encoding utf8 -filepath shell-runner-with-docker-install.ps1\" && powershell -ExecutionPolicy Unrestricted -File shell-runner-with-docker-install.ps1 -GitLabURL ${data.template_file.shell-runner-with-docker-install.vars.GitLabURL} -GitLabRunnerTokenList ${data.template_file.shell-runner-with-docker-install.vars.GitLabRunnerTokenList} -external_access ${data.template_file.shell-runner-with-docker-install.vars.external_access}"

#"commandToExecute": "powershell -command \"[System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String('${base64gzip(data.template_file.shell-runner-with-docker-install.rendered)}')); $gs=[IO.Compression.GzipStream]::New($is, [IO.Compression.CompressionMode]::Decompress); $r=[IO.StreamReader]::New($gs, [System.Text.Encoding]::UTF8); Set-Content shell-runner-with-docker-install.ps1 $r.ReadToEnd(); $r.Close();\" && powershell -ExecutionPolicy Unrestricted -File shell-runner-with-docker-install.ps1 -GitLabURL ${data.template_file.shell-runner-with-docker-install.vars.GitLabURL} -GitLabRunnerTokenList ${data.template_file.shell-runner-with-docker-install.vars.GitLabRunnerTokenList} -external_access ${data.template_file.shell-runner-with-docker-install.vars.external_access}"

#$gs=[IO.Compression.GzipStream]::New($is, [IO.Compression.CompressionMode]::Decompress); $r=[IO.StreamReader]::New($gs, [System.Text.Encoding]::UTF8); Set-Content shell-runner-with-docker-install.ps1 $r.ReadToEnd(); $r.Close();

#"commandToExecute": "powershell -ExecutionPolicy Unrestricted -Command $is=[IO.MemoryStream]::New([System.Convert]::FromBase64String(\\\"${base64gzip(file("${path.module}/startup-script.ps1"))}\\\")); $gs=[IO.Compression.GzipStream]::New($is, [IO.Compression.CompressionMode]::Decompress); $r=[IO.StreamReader]::New($gs, [System.Text.Encoding]::UTF8); Set-Content \\\"C:\\Windows\\Temp\\startup-script.ps1\\\" $r.ReadToEnd(); $r.Close(); .\\\"C:\\Windows\\Temp\\startup-script.ps1\\\" -parameterX \\\"${var.some_value}\\\""

#This code is inline - and not stored in a disk file - because it seems very difficult to get the file encoding and EOL characters
# correct when it is created as a file on non-windows systems (e.g. vs-code Web, Mac, Linux)

data "template_file" "shell-runner-with-docker-install" {
    template = "${file("shell-runner-with-docker-install.ps1")}"
    #template = <<EOF
#EOF

    vars = {
        GitLabURL              = "${var.GitLabURL}"
        GitLabRegistryURI      = "${var.GitLabRegistryURI}"
        GitLabRunnerTokenList  = "${var.GitLabRunnerTokenList}"
        external_access        = "${var.external_access}"
  }
} 

