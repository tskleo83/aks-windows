variable "resource_group" {
    type = string
    description = "Name of the resource group to create"
    default = "gitlab-aks-windows-rg"
}

variable "location" {
    type = string
    description = "Location of the resources"
    default = "westus"
}

variable "node_count_linux" {
    type = number
    description = "Number of Linux nodes"
    default = 1
}

variable "node_count_windows" {
    type = number
    description = "Number of Windows nodes"
    default = 1
}

#Major version of azure_tf_provider_windows_os_sku and azurerm_windows_virtual_machine_sku should match
variable "azure_tf_provider_windows_os_sku" {
    type = string
    description = "Windows2022 or Windows2019, docs: https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/kubernetes_cluster_node_pool#os_sku. Major version of azure_tf_provider_windows_os_sku and azurerm_windows_virtual_machine_sku should match."
    default = "Windows2022"
}

#Major version of azure_tf_provider_windows_os_sku and azurerm_windows_virtual_machine_sku should match
variable "azurerm_windows_virtual_machine_sku" {
    type = string
    description = "2022-Datacenter or 2019-Datacenter, others: 'Get-AzVMImageSku -Location eastus -PublisherName MicrosoftWindowsServer -Offer WindowsServer | Select Skus' Major version of azure_tf_provider_windows_os_sku and azurerm_windows_virtual_machine_sku should match."
    default = "2022-Datacenter"
}

variable "windows_node_vmtype" {
    type = string
    description = "VM selection also controls whether you have amd or arm CPUs for Windows node pool. docs: https://azure.microsoft.com/en-us/pricing/details/virtual-machines/series/"
    default = "Standard_D2s_v4"
}

#GitLab Shell Runner With Docker
variable "win_username" {
  description = "Windows node username"
  type        = string
  sensitive   = false
}

variable "win_userpass" {
  description = "Windows node password (must meet default complexity requirements for Windows version you are deploying)"
  type        = string
  sensitive   = true
}

variable GitLabURL {
    type = string
    description = "GitLab instance URL for runner registration"
    default = "https://gitlab.com"
}

variable GitLabRegistryURI {
    type = string
    description = "GitLab registry URI - this might be the regular gitlab instance uri with a port or a special DNS host. It is configurable on self-managed instances."
    default = "registry.gitlab.com"
}

variable GitLabRunnerTokenList {
    type = string
    description = "Semi-colon (;) delimited list of GitLab instance, group or project runner tokens to setup runners for - usually only one at the highest group level the runner will service. "
}

#For development only

variable external_access {
    type = bool
    description = "Whether to setup RDP Access to the GL Runner for debugging ('true' or 'false' or 1 or 0)"
}

variable disable_create_cluster {
    type        = bool
    description = "Disable cluster setup to make testing other elements faster ('true' or 'false')"
    default     = false
}