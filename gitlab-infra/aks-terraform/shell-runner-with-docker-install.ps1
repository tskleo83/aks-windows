﻿[CmdletBinding()]

param 
( 
    [Parameter(ValuefromPipeline=$true,Mandatory=$true)] [string]$GitLabURL,
    [Parameter(ValuefromPipeline=$true,Mandatory=$true)] [string]$GitLabRegistryURI,
    [Parameter(ValuefromPipeline=$true,Mandatory=$true)] [string]$GitLabRunnerTokenList,
    [Parameter(ValuefromPipeline=$true,Mandatory=$false)] [string]$external_access='False'
)

$external_access = [bool]$external_access #configuring parameter as string and casting to bool allows the following to be passed: 0/1 (as number or string), 'true'/'false' (in any character case)

Function logit ($Msg, $MsgType='Information', $ID='1') {
    Write-Host "[$(Get-date -format 'yyyy-MM-dd HH:mm:ss zzz')] $MsgType : From: $SourcePathName : $Msg"
}

logit "Starting transcript in C:/Users/Public/azurerm_virtual_machine_extension/initialprovisioning.log"
Start-Transcript $env:public\azurerm_virtual_machine_extension\initialprovisioning.log

If ($external_access -eq $True) {
    logit "'external_access' is 'true', Installing SSH..."
    Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0
    Start-Service sshd
    Set-Service -Name sshd -StartupType 'Automatic'
}

logit "`n`nInstalling requirements for all GitLab Runners"

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12,[Net.SecurityProtocolType]::Tls11; 

if (!(Test-Path env:chocolateyinstall)) {
    logit "Installing Chocolatey"
    Invoke-WebRequest https://chocolatey.org/install.ps1 -UseBasicParsing | Invoke-Expression
}

If (!([bool](get-command git -ErrorAction SilentlyContinue))) {
    logit "`nInstalling git"
    choco install -y git  -params '"/GitOnlyOnPath"' -force --no-progress
    #Enable using git in this process
    $gitpath = 'C:\Program Files\git\cmd'
    $CurrentProcessPath = [Environment]::GetEnvironmentVariable("Path", "Process")
    if (!($CurrentProcessPath -ilike "*\git\cmd*"))
    {
        [Environment]::SetEnvironmentVariable("Path", $CurrentProcessPath + ";$gitpath", "Process")
    }
}

If (!([bool](get-command pwsh -ErrorAction SilentlyContinue))) {
  logit "Installing PowerShell 7 or later (pwsh)"
  choco install -y git powershell-core --no-progress
}

logit "Installing GitLab Runner..."

$RunnerInstallRoot='C:\GitLab-Runner'
$RunnerConfigToml="$RunnerInstallRoot\config.toml"
$GITLABRunnerExecutor='shell'
$GITLABRunnerConcurrentJobs=4

#For depr. Runner REGISTRATION Tokens
$RunnerCompleteTagList = "Windows", "glexecutor-$GITLABRunnerExecutor", $GITLABRunnerTagList -join ','
$RunnerCompleteTagList = "`"$($RunnerCompleteTagList.trimend(","))`""
$GITLABRunnerVersion='latest'
$RunnerName="Windows-Shell-with-Docker-for-AKS"

if (!(Test-Path $RunnerInstallRoot)) {New-Item -ItemType Directory -Path $RunnerInstallRoot}
#Most broadly compatible way to download file in PowerShell
If (!(Test-Path "$RunnerInstallRoot\gitlab-runner.exe")) {
  (New-Object System.Net.WebClient).DownloadFile("https://gitlab-runner-downloads.s3.amazonaws.com/$($GITLABRunnerVersion.tolower())/binaries/gitlab-runner-windows-amd64.exe", "$RunnerInstallRoot\gitlab-runner.exe")
}

#Write runner config.toml
set-content $env:public\config.toml -Value @"
concurrent = 4
log_level = "warning"
"@

pushd $RunnerInstallRoot
.\gitlab-runner.exe install

foreach ($RunnerToken in $GitLabRunnerTokenList.split(';')) {

  if ($RunnerToken -ilike 'glrt-*') {
      $TokenParameters = "--token $RunnerToken"
  } else {
      $TokenParameters = "--registration-token $RunnerToken --tag-list $RunnerCompleteTagList --locked=false"
  }

  logit "Running .\gitlab-runner.exe register --config $RunnerConfigToml --name $RunnerName --non-interactive --url $GitLabURL $TokenParameters --executor $GITLABRunnerExecutor"

  Start-Process .\gitlab-runner.exe -Wait -ArgumentList "register --config $RunnerConfigToml --name $RunnerName --non-interactive --url $GitLabURL --executor $GITLABRunnerExecutor $TokenParameters"
}

(Get-Content $RunnerConfigToml -raw) -replace '(?m)^\s*concurrent.*', "concurrent = $GITLABRunnerConcurrentJobs" | Set-Content $RunnerConfigToml

if (![bool](Get-Command "pwsh" -ErrorAction 0) -AND (Get-Content $RunnerConfigToml -raw) -notmatch '(?m)shell\s*=\s*"powershell".*') {
  logit "PowerShell Core/7 or later not found, updating default shell to Windows PowerShell"
  (Get-Content $RunnerConfigToml -raw) -replace '(?m)shell\s*=.*', 'shell = "powershell"' | Set-Content $RunnerConfigToml
}

.\gitlab-runner.exe start

popd

logit "Restarting the Runner to update with any configuration changes"
cd $RunnerInstallRoot
.\gitlab-runner.exe stop
.\gitlab-runner.exe start


If (!([bool](get-command docker -ErrorAction SilentlyContinue))) {
  logit "Installing Docker..."
  choco install -y Containers --source windowsfeatures
  choco install -y docker-engine
  net localgroup docker-users gitlab-runner /ADD
  #Avoid docker error "Skipping Foreign Layers" on docker push
  $obj2 =$(cat c:\programdata\docker\config\daemon.json | convertfrom-json)
  $obj2 | add-member -Name "allow-nondistributable-artifacts" -Value @("registry.gitlab.com") -Membertype NoteProperty
  $obj2 | convertto-json | out-file c:\programdata\docker\config\daemon.json -encoding utf8
  sc.exe config docker start= delayed-auto 
  sc.exe failure docker reset= 0 actions= restart/0/restart/5000/restart/15000
}

Stop-Transcript

#reboot 4 docker & pwsh 7
logit "Rebooting in ten seconds"

#clean orchestrator exit
Start-Process "shutdown.exe" -Args " /r /t 10" -WindowStyle Hidden

exit 0