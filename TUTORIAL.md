# Summary of Infrastructure Configuration

The following diagram displays a sample minimalist configuration (least config). The GitLab instance is just for reference and not deployed as part of the working Infrastructure as Code.

 There are two switches for helping while you are building and debugging this for yourself. When working only to troubleshoot the Windows VM runner, the generation of the AKS cluster can be disabled. Additionally, debugging the runner build frequently requires logging into the runner vm. Another switch enables the ports and services to be able to SSH to the instance - but only from CloudShell in order to keep it a bit more secure.

![img](images/labbuild.png)

## Prerequisites

1. An Azure account and subscription.
2. A new GitLab Group to work in a GitLab Instance (could be gitlab.com or a self-managed instance).
  3. The GitLab instance must be contactable from the Azure account you are working in. A non-public, self-managed instance may not be able to receive network communications from a given Azure account. The GitLab instance does NOT initiate conversations with GitLab Runner nor GitLab Agent for Kubernetes - so only the ability to initiate conversations with the GitLab instance from the Azure environment is needed (and return conversations - aka "Stateful Firewalling")
  4. Your GitLab instance must be able to retrieve the example project from [https://gitlab.com](https://gitlab.com/). An alternative is to clone the repository from GitLab.com to a local repository and then push it to a private GitLab instance on a machine that has access to both.
  5. You must have maintainer or owner permissions to the group.
  6. Your group must be licensed for GitLab Ultimate to see security findings.
  7. If any of the three of these cannot be done on your regular GitLab instance or group, consider using a free user id to create a new namespace at the root of gitlab.com. In the new group you can enable a free ultimate trial for the security scanning features by clicking "Settings" => "Billing" => "Enable free Ultimate trial" (button at bottom center).

Create the group and write it's full path name (without the instance URL) here: _____________

Write down the URL to your GitLab instance ([https://gitlab.com](https://gitlab.com/) for GitLab SaaS): ____________. Whenever the tutorials reference "GitLab Instance" this is the instance they mean.

1. As a preparatory step, you will need to collect some runner registration tokens from the GitLab UI within this group. These will be used in later automation steps to setup runners. This automation can use either a legacy "[Runner Registration Token](https://docs.gitlab.com/ee/ci/runners/runners_scope.html#create-a-shared-runner-with-a-registration-token-deprecated)" or a newer "[Runner Authentication Token](https://docs.gitlab.com/ee/ci/runners/runners_scope.html#create-a-shared-runner-with-an-authentication-token)". If you do the newer one, you will specify your tags at the time you create the token in the GitLab UI. Since tags are prebaked into Runner Authentication Tokens, you need to create one for each of the runners we'll be using. Follow the below procedure once for each line in this table - copy the tokens from the UI to a temporary place as they only display once. Using the indicated tags is important because this is how specific pipeline jobs will be routed to the appropriate runner.

| **Runner Description**     | **Platform** | **Tags**               | **Run Untagged Checkbox** |
| -------------------------- | ------------ | ---------------------- | ------------------------- |
| Windows Shell for Docker   | Windows      | Windows, Shell, Docker | unchecked                 |
| K8s for Linux Containers   | Linux        |                        |                           |
| (not "Kubernetes")         | aks-linux    | checked                |                           |
| K8s for Windows Containers | Linux\*      |                        |                           |
| (not "Kubernetes")         | aks-windows  | unchecked              |                           |

1. In the group you've created, on the left navigation, click "build" and then click "Runners"
2. In the upper right corner click "New group runner"
3. Complete the details according to the table row above.
4. Click "Create runner"
5. Copy the token displayed on the UI - be sure you know which token is for which row above
6. Azure CloudShell pre-configured for access to your subscription. This helps avoid deep rabbit holes of adjusting your current development environment to be able to run the code and makes azure access somewhat more secure.
  7. Our sample application will be Microsofts [ASP.NET Docker Sample](https://github.com/microsoft/dotnet-framework-docker/blob/main/samples/aspnetapp/README.md) because:
        8. It is implemented as a Windows container.
            9. It is already built and available in docker hub as a container, which allows easy deployment testing of a known working container.
                10. It has a docker oriented build process for us to experiment with building a custom version to a Windows container using a GitLab runner capable of Windows container builds.
                    11. It contains .NET source code and NuGet packages to test security scanning and the code has some medium vulnerabilities.
                    12. It uses legacy .NET 4.x runtimes - a known requirement for implementing .NET specifically in Windows containers.

# Tutorial Sequencing

1. **Tutorial 1 Setting Up the Envronment** sets up an AKS cluster, all networking and a GitLab Windows Shell Runner with Docker.
2. **Tutorial 2 Deploy GitLab Agent to AKS Cluster** - sets up the agent in the cluster.
3. **Tutorial 3: Deploying Kubernetes App to Cluster** - adds monitoring of our applications "deploymanifests" folder. This will deploy the public ASP.NET Docker Sample image.
4. **Tutorial 4: Build A Windows Docker Image** - with the same source as the public image, build a custom copy of the image stored in GitLab Registry.
5. Tutorial 5: Deploy The Custom Image To the Cluster.

# Tutorial 1 Steps: Setting Up The Environment

If you have not used Azure CloudShell on this Azure account, follow this quickstart guide to have it provisioned and configured with access to a subscription: [Quickstart for Azure Cloud Shell](https://learn.microsoft.com/en-us/azure/cloud-shell/quickstart?tabs=azurecli). Configure for the Bash shell.
 Keyboard Time: 15 mins
 Automation Wait Time: 40 mins

1. Open Azure CloudShell with a Bash Shell.
2. `script ~\consoletranscript.log #in case CloudShell times out, you will have a log of final results`
3. `cd clouddrive`
4. `git clone https://gitlab.com/guided-explorations/microsoft/azure-aks/aks-windows.git`
5. `cd aks-windows/gitlab-infra/aks-terraform`
6. `terraform init`
7. `terraform validate`

The next step will prompt for several values, here are the values.

| **variable prompt**              | **value**                                                |
| -------------------------------- | -------------------------------------------------------- |
| var.GitLabRunnerTokenList        | "Windows Shell for Docker" token from above.             |
| var.external_access             | true                                                     |
| Only used for troubleshooting.   |                                                          |
| var.win_username                | gitlab-runner                                            |
| var.win_userpass                | Complexity: 10 chars, letters, 1 number, 1 special char. |
| "Testingit1!" is known to work 🙂 |                                                          |
| Only used for troubleshooting.   |                                                          |

8. `terraform apply -auto-approve`
   #if you do a lot of testing, create a terraform.tfvars file to suppress the prompts

Configuring kubectl and testing permissions and connectivity.

9. `az aks get-credentials --resource-group gitlab-aks-windows-rg --name glaks1 --file $HOME/azurek8s`
10. `echo 'export KUBECONFIG=$HOME/azurek8s' | tee -a $HOME/.bash_profile`
11. `export KUBECONFIG=$HOME/azurek8s`
      When your session expires, you may need to rerun this last line if you get kubernetes auth errors using kubectl or helm
12. `kubectl describe node akswin000000 | kubectl describe node akswin000000 | grep "windows-build\|\/arch"`

Note: the next steps test external connectivity from within the cluster.

13. `kubectl run --rm -it bash -n default --image bash bash --overrides='{"apiVersion": "v1", "spec": {"nodeSelector": { "kubernetes.io/os": "linux" }}}'`
14. Assuming successful container launch, Inside container: `wget -qO- [http://ipv4.icanhazip.com](http://ipv4.icanhazip.com/)`
      If you get an IP back, you are wired to the outside world!
15. `exit`

Installing Two GitLab Runners Using The Runner Helm Chart

This section installs two GitLab Runners on the Linux node pool - one targets the linux node pool and the other targets the Windows node pool.

Keyboard Time: 10 mins
 Automation Wait Time: 5 mins

16. `cd ~/clouddrive/aks-windows/gitlab-infra/runner-configs`
17. `helm repo add gitlab [https://charts.gitlab.io](https://charts.gitlab.io/)`

Note: If you have not previously used the Nano editor, in order to save and exit, **press** CTRL-X and **Type** y and **press** Enter to save.

18. `nano k8s-win-runner.yaml - replace <YOUR RUNNER TOKEN> with the runner token retrieved in the data collection stage described as "K8s for Windows Containers"`
19. `helm install --namespace gitlab-runner --create-namespace win-runner -f k8s-win-runner.yaml gitlab/gitlab-runner --set nodeSelector."kubernetes\\.io/os"=linux`
      **IMPORTANT** The runner targeting the windows node pool runs on linux - do not change this portion of the helm command.
20. `nano ../runner-configs/k8s-lin-runner.yaml - replace <YOUR RUNNER TOKEN> with the runner token retrieved in the data collection stage described as "K8s for Linux Containers"`
21. `helm install --namespace gitlab-runner --create-namespace lin-runner -f k8s-lin-runner.yaml gitlab/gitlab-runner --set nodeSelector."kubernetes\\.io/os"=linux`
22. `kubectl get deployments -A`

Note: You should see lin-runner-gitlab-runner and win-runner-gitlab-runner in the namespace gitlab-runner.

23. Navigate to the GitLab group where you created or retrieved the runner token.
24. **Click** Build => Runners
25. All three of the runners you have configured should show "Online" for their status.

# Tutorial 2: Deploying GitLab Agent to AKS Cluster

If the below project import process is not possible due to network security for your GitLab instance, an alternative is to clone the repository from GitLab.com to local repository and then push it to a private GitLab instance on a machine that has access to both.

Keyboard Time: 15 mins
 Automation Wait Time: 5 mins

1. Navigate to the GitLab group where you created or retrieved the runner token.
2. In the upper right corner **click** New project.
3. On the next page, **click** Import project.
4. On the next page, **click** Repository by URL.
5. Under _Git repository URL_, **paste** [https://gitlab.com/guided-explorations/microsoft/azure-aks/aks-windows.git](https://gitlab.com/guided-explorations/microsoft/azure-aks/aks-windows.git)
6. For _Visibility level_, **Click** Public.
7. **Click** Create project
8. Since the example project includes a prebuilt GitLab Agent Configuration file in '.gitlab/agents/aks-agent1/config.yaml' we can immediately register our agent.
9. From the left navigation, **Click** Operate and then **Click** Kubernetes clusters
10. In the upper right, **Click** Connect a cluster (agent)
11. On the next panel, **Click** Select an agent or enter a name to create new
12. From the drop list titled _Select an agent or enter a new name to create new_, **Select** aks-agent1 (this choice is present in the UI because the config.yaml described above is in the correct file location in the repo).
13. **Click** Register
14. Next to the set of helm commands, **Click** the Clipboard icon to copy them.
15. Do not close the panel as you will not be able to retrieve the token if you do.
16. In Azure CloudShell, **Paste** the commands from the clipboard.
     Note: If you get an authentication error, then paste this into the shell console: export KUBECONFIG=$HOME/azurek8s
17. If the command succeeds, type "kubectl get deployments -A" - you should see a new namespace "gitlab-agent-aks-agent1" with a deployment titled something like "aks-agent1-gitlab-agent-v1".
18. Switch back to the GitLab browser tab with the helm commands and **Click** Close.
19. Refresh the tab.
20. If everything is working correctly, you should see a green check and "Connected" in the Connection status column for the agent Named aks-agent1. The Runner version should also be populated as well - if it is not, something may be wrong.

# Tutorial 3: Deploying Kubernetes App to Cluster

You will now edit the agent config.yaml file to configure it to monitor and deploy the manifest files in the deploymanifests subfolder of this project. This will immediately deploy a vanilla public version of the ASP.NET web app.

 NOTE: For simplicity this one GitLab project contains all IaC to setup the AKS cluster and GitLab Runners as well as your application example. In production, these would most likely be in separate repositories since the AKS cluster and GitLab resources for servicing may be shared among applications.

Keyboard Time: 15 mins
 Automation Wait Time: 40 mins

1. Within your newly created project, on the left navigation, **Click** Code => Repository.
2. On the upper right, **Click** Edit (button with small down arrow) => Web IDE
3. In the Web IDE, **click** through the left file navigation to .gitlab/agents/aks-agent1 then **Click** config.yaml (since this is the only file in the tree the directory levels will all open with one click)
4. We will now edit this file. Under ci_access: and groups: for id: update the text _CHANGE_TO_GROUP_PATH_ to be the parent group of your current project.
   For instance, if your project full path is https://gitlab.com/mygroup/mysubgroup/aks-windows then you would use mygroup/mysubgroup
    NOTE: If you are looking at the url in the web ide for reference, do NOT include extraneous path bits such as -/ide/project/ and /edit/master/-
5. Under gitops: and manifest_projects: for id: update the text _CHANGE_TO_PROJECT_PATH_ to be the path to your current project (not the parent group like the last step).
   For instance, if your project full path is https://gitlab.com/mygroup/mysubgroup/aks-windows then you would use mygroup/mysubgroup/aks-windows
    NOTE: Again, If you are looking at the url in the web ide for reference, do NOT include extraneous path bits such as -/ide/project/ and /edit/master/-

Notice: when you commit this agent config file, the GitLab Agent that was just registered will now monitor the webserver.yaml file in the deploymanifests subdirectory of your project. Since it is pointed at a public docker hub image by default, you will end up with an immediate application deployment to the cluster.

6. On the left most icon navigation bar, **Click** the Git icon (three circles with connecting lines)
7. **Click** Commit to 'master' (button)
8. If you receive the message "You're committing your changes to the default branch. Do you want to continue?" **Click** continue
9. In the Azure CloudShell console, type "kubectl get deployments -A"

Note: If you get an authentication error, then paste this into the shell console: export KUBECONFIG=$HOME/azurek8s

10. There should be a deployment called "sample" in the namespace called "default". You may have to run the last command a couple times if things are slow.
11. Keep rerunning the command until you see "1" under the column AVAILABLE in the console output (a full server core container can take some time to become AVAILABLE - "kubectl get pods -A" can be used to watch the various stages of readiness as they occur)
12. **Type** kubectl get services -A
13. For the row with the NAME "sample" copy the value from the column "EXTERNAL-IP"
14. Paste the value into a new browser tab (it should automatically use http://, not https://)
15. You should see the following screen from the default public application.
      ![img](images/webserverscreenshot.png)
16. Click the "Contact" link in the top menu bar to observe the Microsoft contact information.
17. Leave this tab open to check for updates later.

# Tutorial 2: Building a Windows Container Using GitLab CI

This and the following tutorials turn on various pipeline jobs by uncommenting parts of the prebuilt pipeline.

 As noted previously this requires a Window Shell runner with Docker installed - which was provisioned by the Terraform template. Azure Build Services could be used for this part, but it would be important to bring the built container into the GitLab project level container registry in order to take advantage of all security scanning and dynamic DevOps environments functionality.

This example project includes a slightly modified copy of the source for the public container you just deployed to the cluster. By deploying a custom version we can see how a development cycle updates the environment.

1. From the left navigation Click "Code => Repository", then "Edit => Web IDE" (if you reuse a browser tab that has the web IDE already loaded, be sure it is on the correct branch and has the latest update from that branch - if unsure, always open a new one and close old ones).
2. On the left file navigation click the file .gitlab-ci.yml (at the root of the repository).
3. Notice that the first keyword ".build:" has a dot before the word "build". This is similar to a comment.
4. Remove only the "."
5. Notice the tags: section. These three tags (Windows, Shell, Docker) route just this job to the GitLab Windows Shell Runner with Docker installed that terraform built. The same tags were used to build the runner.
6. On the left most icon navigation bar, **Click** the Git icon (three dots with connecting lines)
7. **Click** Commit to 'master' (button)
8. If you receive the message "You're committing your changes to the default branch. Do you want to continue?" **Click** continue
9. The change you just made will trigger a pipeline execution.
10. In a browser tab, navigate to your GitLab Project.
11. On the left navigation **Click** Build => Pipelines
12. A pipeline should already be running, but if one is not:
    a. In the upper right click "Run pipeline"
    b. On the next page, click "Run pipeline"
13. Watch for the "Build" job to complete. (due to the size of the container, the very first build takes some time - after which image caching will make new builds much faster)
14. When the "Build" job shows success, from the left navigation **Click** Deploy => Container Registry
15. You should see an image name with the same name as your repository slug, if you left the repository name as the default when importing the project, this would be "aks-windows"
16. Click the image name to see the tags.
17. If just this one build has been performed, you should see a commit sha tag and a "latest" tag with the same value for "digest".

# Tutorial 3: GitLab Security Scanning

In this section you will use a Merge Request to enable security scanning. GitLab Security Scanning is unique in that the MR becomes a Single Source of Truth for the security findings a developer introduces during their feature development.

1. While in your project, on the left navigation, **Click** Plan => Issues
2. **Click** New issue.
3. In Title, **Type** Security Scanning Enabled and near the bottom **Click** Create issue
4. On the new issue, near the upper right **Click** Create merge request
5. On the New merge request page, near the bottom, **Click** Create merge request
   **Note:** This also created a branch for you to work on your code.
6. On Merge Request **Click** Code => Open in Web IDE
7. On the left file navigation pane, **Click**.gitlab-ci.yml
8. Find the line "include:"
9. For each item under "include:" that has a comment character ("#"), remove the comment character ONLY. (spaces on lines must be preserved and the dash on each line should align with the first line that was not commented)
10. When done, all lines under "include:" should start with two spaces and a dash like the one that was not commented.
11. On the left most icon navigation bar, **Click** the Git icon (three circles with connecting lines)
12. **Click** Commit to '1-security-scanning-ena…' (the number may differ if this was not your first MR in this project).
13. Click the previous web browser tab - which should still show the merge request. (Otherwise from the project navigate to Code => Merge Requests and **Click** the only merge request (should be #1)
14. Near the title of the Merge Request (not in the left navigation), **Click** Pipelines
15. Wait for the latest pipeline to show "passed" (refresh the browser tab to update status).
16. Near the Merge Request title, **Click** Overview

Note: If your group is not licensed for Ultimate or a trial, you will not see the security findings or licenses in the MR in the next steps. The presentation of security findings in this context dramatically lowers the cost of being DevSecOps mature.

17. Refresh the browser. You should see new sections for "License Compliance…" and "Security Scanning…". If not, keep refreshing until they appear.
18. A new section will appear in the merge request starting with "License Scanning detected…", at the right of that text, **Click** the expansion down arrow
      You should see something like this when expanded:
      ![img](images/licensescanresultsinmr.png)
19. A new section will appear in the merge request starting with "Security scanning detected…", at the right of that text, **Click** the expansion down arrow
      You should see something like this:
      ![img](images/securityscanresultsinmr.png)
20. Click on the blue text on one of the vulnerabilities.
21. Notice all the rich information given. You can create an issue with this information for further collaboration.
22. Next to the text Dismiss vulnerability, **Click** the talk bubble icon
23. In "Add a comment or reason for dismissal" **Type** Not relevant to how this component is used
24. **Click** Add comment & dismiss

Notice: The "Dismissed" tag next to the vulnerability.

25. **Click** Mark as ready (button) and the "Merge" button will appear.

 Note: If we had a threshold based approval rule that requires security approval for these Medium level vulnerabilities - we would not be able to take the next step to merge to a shared branch unless someone from Security approved - at that time they could assess our dismissal reason.

26. **Click** Merge

Note: A new merge pipeline is running on the default branch and noted in the UI of the MR.

27. Under the Merged by heading there should be a new pipeline running on master for your merged results, **Click** <the pipeline number> to see the pipeline progress.

 Note: If your group is not licensed for Ultimate or a trial, you will not see the security findings in the security Dashboard This dashboard data flows upwards to all upbound groups so that a more comprehensive view of multiple projects is available at every higher level group.

28. When the pipeline is complete, on the left navigation, **Click** Secure => Vulnerability Report

Note: If we had taken a peek before merge, there would have been no findings here yet - because they were all on the branch. Now that we merged to the default branch, these findings are tracked as tech debt in our released code and can be managed in this security dashboard to create issues to address them. All security dashboard findings also flow up the group hierarchy for managing the vulnerabilities across multiple project contexts.

29. When the pipeline is complete, on the left navigation, **Click** Secure => Dependency list

Note: This output is due to the fact our pipeline runs GitLab Dependency Scanning which builds this part of the Software Bill of Materials (SBOM) as well as scans for vulnerable dependencies.

30. **Click** Secure => Policies

This is where you can configure policies for security scanning. They can be stored in a seperate project and enforced on multiple projects. You are able to fail at either the CI Scanning stage or simply require special approvals. You can configure very complex criteria for how many of each severity level and from which scanners are allowed before requiring approval or failing the pipeline.

# Tutorial 4: GitOps Pull Deployment Using the GitLab Agent

This part of the workflow is somewhat complex because it involves updating a file in the repository we are running in. This is necessary because GitOps Pull agents like the GitLab Agent simply monitor a Kubernetes Deployment Manifest in a git repository. So the deployment step is to simply update the manifest and wait for the agent to do its magic. Most of the complexity is hidden in a GitLab CI include.

 You will also need to create project access token and populate a couple variables so that your CI job will be allowed to write back to the repository it is running from.

 Note: Project Access Tokens are only available with licensed GitLab (not even a trial will have this feature) - you can use a Personal Access Token instead to keep working, but be careful it is not exposed to anyone.

1. While in your projects, on the left navigation, **Click** Settings => Access Tokens
2. Under 'Add a project access token', for Token name, Type WriteRepository
3. IMPORTANT: Under 'Select a role', **Select** Maintainer
4. Under 'Select scopes'
    a. Select read_repository (DO NOT SELECT read_registry)
    b. Select write_repository(DO NOT SELECT write_registry)
5. Click Create project access token (button)

**Page Reloads With Update - Don't Close**
 Notice the same page reloads, but at the top of the screen now has a grey box containing the token information. IMPORTANT - Do not navigate to another page in this browser as this is the only time you can see the token. You will have to create a new token if you leave the page.

6. Use copy and paste to record the following in a temporary document (do not hand type tokens):
7. PROJECT_COMMIT_TOKEN = [project access token from UI]
8. On the left navigation, **Click** Manage => Members
9.  In the search prompt **Type** WriteRepository
10. The user list should return one entry
11. In the listing, under "WriteRepository", copy the user name that starts with "project_" and ends with "_bot" - do not include the @ sign.
12. In the previous temporary document, record:
13. PROJECT_COMMIT_USER = [the user id you just copied]

 On the next steps take care to use the exact variable names and that the token and user name go into the correctly named variables.

14. On the left navigation, **Click** Settings => CI/CD
15. To the right of 'Variables', **Click** Expand
16. **Click** Add variable
17. For "Key", **Type** PROJECT_COMMIT_TOKEN
18. In the Value field Copy and Paste [the temporary document value for PROJECT_COMMIT_TOKEN]
19. Under Flags, **Deselect** Protect variable
20. Under Flags, **Select** Mask variable
21. **Click** Add variable
22. To add another variable, **Click** Add variable
23. For Key, **Type** PROJECT_COMMIT_USER
24. In the Value field Copy and Paste [the temporary document value for PROJECT_COMMIT_USER]
25. Under Flags, **Deselect** Protect variable
26. **Click** Add variable

27. From the left navigation **Click** Code => Repository, then "Edit => Web IDE"
    **DO NOT** reuse the last Web IDE tab as it is likely pointing at the branch we just merged and deleted.
28. On the left file navigation click the file .gitlab-ci.yml (at the root of the repository).
29. Notice that the job keyword ".update_manifest_with_version:" has a dot before the word "update_". This is similar to a comment.
30. Remove only the "."
31. On the left most icon navigation bar, **Click** the Git icon (three circles with connecting lines)
32. **Click** Commit to 'master' (button)
33. If you receive the message "You're committing your changes to the default branch. Do you want to continue?" **Click** continue
34. The change you just made will trigger a pipeline execution.
35. Keep the Web IDE window open if you wish to reuse it rather than reopening it each time we need to edit.
36. In a browser tab, navigate to your GitLab Project.
37. On the left navigation **Click** Build => Pipelines
38. A pipeline should already be running, but if one is not:
    a. In the upper right click "Run pipeline"
    b. On th next page, click "Run pipeline"
39. **Click** <the pipeline status bubble> to see the jobs.
40. Watch for the build stage and test stage jobs to complete.
41. The update_manifest_with_version job will not run because it is a manual job.
42. Once the build and test stages have completed, Inside the job bubble titled "update_manifest_with_version" **Click** <the play icon>
     Note: You may have to vertically scroll to see the play icon.
43. While that is processing, from the left navigation **Click** Deploy => Container Registry
44. You should see an image name with the same name as your repository slug, if you left the repository name as the default when importing the project, this would be "aks-windows"
45. Click the image name to see the tags.
46. Find the "latest" tag.
47. If you want you can find the commit sha tag with the same digest so you can verify the manifest was updated. If you find it, make note of it or copy it.
48. From the left navigation **Click** Code => Repository
     Note: DO NOT reuse the Web IDE because our CI/CD job will have done a commit back to the repository
49. In the file list **Click** deploymanifests => webserver.yaml
50. Find the line starting with "image:"
51. The tag at the very end of the line should match the one that has the same Digest as the "latest" tag.
52. If you still have the deployed Website open form the second exercise, refresh the page.
    a. If you do not have that browser tab available, go to the Azure CloudShell session and type "kubectl get services -A"
    b. For the row with the NAME "sample" copy the value from the column "EXTERNAL-IP"
    c. Paste the IP in a browser Window.
53. The web page should now have the Built using GitLab notation:
    ![img](images/webserverscreenshotwithgitlab.png)
54. Click the "Contact" link in the top menu bar to observe the GitLab contact information.

# Final Tutorial 5: Teardown

Before tearing down, consult the appendix for optional tutorial exercises for other use cases.

1. Open Azure CloudShell with a Bash Shell.

 Note: If you get an authentication error in the next step, then paste this into the shell console: export KUBECONFIG=$HOME/azurek8s

2. `script ~\teardown.log #in case CloudShell times out, you will have a log of final results`
3. `cd clouddrive/aks-windows/gitlab-infra/aks-terraform`
4. `terraform destroy -auto-approve`

Note: The teardown depends on the \*.tfstate files that match the deployment. If these are missing or teardown errors out, simply delete the entire resource group named gitlab-aks-windows-rg in the Azure console.

5. To delete the GitLab Project, while in the project **Click** Settings => General
6. Next to _Advanced_, **Click** Expand
7. At the bottom of the page, **Click** Delete project
8. On the next panel, copy and paste the project path into the confirmation prompt and **Click** Yes, delete project

# Fully Working Example Code and Tutorial Maintenance

The how to section of this post as well as all the code and snippets needed to stand up a working example are available and will be updated with needed changes and fixes at [https://gitlab.com/guided-explorations/microsoft/azure-aks/aks-windows](https://gitlab.com/guided-explorations/microsoft/azure-aks/aks-windows.git)

# Conclusion

We have discussed and provided working terraform code to provision the essential GitLab Infrastructure for the complete lifecycle of development and deployment of Windows containers to the Azure Kubernetes Service (AKS).

- We have maximized the use of AKS by installing GitLab Runner for both Linux and Windows nodepools as well as installed the GitLab Agent for Kubernetes in order to show the GitOps Pull Deployment method of deploying to Kubernetes.

- We have chosen to use a Windows VM runner for Windows container builds, but you could also create an alternate implementation that uses Azure build services for that portion.

- One of the optional tutorials also shows that GitLab Agent for Kubernetes enables full CD push to AKS if that is your deployment methodology. This is also how GitLab Auto DevOps would work in a Kubernetes deployment workflow, even if the final push to production was done by pulling manifests.

# Appendices

## Optional Self-Guided Tutorial: Full Development Lifecycle Change

This tutorial does not give step-by-step instructions - instead, please use previous instructions to figure out the detailed steps. If you use an issue and Merge Request to make these changes, you will emulate how actual changes happen - but it will take a little longer.

1. Use the Web IDE to edit the file src-aspnet-windows/aspnetapp/Content/Site.css and add the following on a new line inside the body { } section:

 background-color:lightsalmon;

2. Use the Web IDE git panel to commit your change.
3. Watch for the build stage and test stage jobs to complete.
4. Once the build and test stages have completed, Inside the job bubble titled "update_manifest_with_version" **Click** <the play icon>
5. Open the browser to the website (if you don't have an existing tab use "kubectl get services -A' to find the IP again) and see that it now has a light orange background.

## Optional Self-Guided Tutorial: See How To Use Push CD To Kubernetes Through the Secure GitLab Agent connection

The previous tutorials only showed using GitLab Agent for Kubernetes in a Pull CD mode and with basic manifests. GitLab Agent also has flux integrated. In addition, if your Kubernetes deployment practices require the usage of cluster management commands, the secure agent connection can be used to perform this type of deployment. If you insert the below code into the working .gitlab-ci.yml you can see that the $KUBE_CONTEXT environment variable is populated by the pipeline to give access to the specific cluster (and security context) of the installed agent. Consult Tutorial 3 to determine the substitution value of _CHANGE_TO_PROJECT_PATH_ in the below code. This code is also present in the project you are working on under gitlab-ci-libs/otherexamples.ci.yml

```yamltest_cd_push:
test_cd_push:
  image:
    name: bitnami/kubectl:latest
    entrypoint: ['']
  stage: build
  variables:
    KUBE_CONTEXT: '_CHANGE_TO_PROJECT_PATH_:aks-agent1'
  script:
    - |
      kubectl config get-contexts
      kubectl config use-context ${KUBE_CONTEXT}
      kubectl get deployments -A
      kubectl get services -A
```

## Optional Self-Guided Tutorial: Running Official PowerShell Container for Nano Server, Server Core and Linux

There is an 3 job example of using the cluster to run three types of Microsoft maintained containers for Nano and Server Core (on the windows node pool) and a maintained Microsoft Linux container.

The code for the three example jobs is present under the project file path: gitlab-ci-libs/otherexamples.ci.yml

## Reusing for Production

Here are some additional guidelines if you wish to reuse this code for production templates:

- Because they are passed as a variable, Azure Custom Scripts have a length limit - the existing script is very close to that limit, so you'll have to be careful. There is a more elaborate method available where longer or more scripts are stored in Azure storage. You can also use curl to pull remote scripts onto the VM.
- There are some rough, but fairly comprehensive debugging tips and command lines in https://gitlab.com/guided-explorations/microsoft/azure-aks/aks-windows/-/blob/master/DEBUGGING.md

## Pod Scheduling On OS Based Node Groups

Understanding nodeselectors, taints and tolerances is very important when two completely different operating systems node pools are in a single Kubernetes cluster.

The approach taken in this example is to default the entire cluster to run things on the Linux Node Pool - this means that all GitLab documentation on K8s Operations, GitLab Runner and Agent - including troubleshooting information - can be used directly without having to do node selection logic for each command. Defaulting the cluster is simply done by marking the windows node pool with a "NoSchedule" taint which then ensures everything that is specified without nodeSelectors runs on Linux. This is a common general pattern on AKS clusters even when their primary workload is Windows Containers.

Once the cluster defaults to linux node pools, Kubernetes/Flux deployment manifests can be configured with node selectors to allow the Linux GitLab Agent for Kubernetes to target Windows node pools.

For the GitLab Runner Kubernetes Executor, the nodeselector and tolerances are usually in the runner config.toml. Using config.toml results in a runner instance only deploying to an OS specific node pool. A runner tag would then be used to indicate which node type is targeted. This makes routing by runner tags easy for CI Pipeline engineers to understand.

Another approach is to enable "overrides" in the linux k8s runner config.toml and then CI environment variables can be used to target the windows node pool for just specific jobs. This trades off a little more difficult per-job configuration for less runner sprawl. In general, increasing a developer's daily thinking burden by relying on detail CI configuration can create a scaled human work activity - which is quite literally "de-automation". However, if you are using runner tags for many other things, it might be burdensome to also have a permutation for OS selection.

A word of caution. In order to keep your experimentation costs low, this example uses a single AKS cluster and only one Windows and one Linux node pool to run all GitLab Infrastructure and run sample applications.

In the real world, runners have their own workload shape and should not be mixed in with Application workloads. This configuration might also confuse some readers into thinking tha GitLab Runners AND Agents are required in every Application cluster. In reality, Application clusters only need the GitLab Agent for Kubernetes configured to fully enable GitLab deployment management of the cluster.

## Common Known Root Causes from Errors

- `ERROR: Job failed (system failure): unable to upgrade connection: container not found ("build")` 
  - can mean that you are trying to run on a container that does not have `pwsh` installed.
  - can mean that you have a mismatch of major Windows version between the requested container and the Windows node pool - for example executing a 2022 container on a 2019 windows node pool.