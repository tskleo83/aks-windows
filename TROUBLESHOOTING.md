# AKS Windows

# Configuration
- This configuration defaults to amd64 with Windows 2022. Commented lines show changes required to change either of these decisions.


# Major Notations
- GitLab Runner only targets compatibility with the pwsh (PowerShell 7) shell. 
  - Since it is not be installed by default on Nano and Full Windows containers prepared by Microsfot, the [Microsoft PowerShell containers](https://hub.docker.com/_/microsoft-powershell) work best for CI. 
  - The only image for 2019 in this container registry appear to be for nano - which means you'll need to create custom containers for 2019 for Windows editions besides nano.
  - The official docker hello world image for nano uses CMD shell - so requires a special runner configured for that shell in order to be used.
- When using a Dockerfile to build a windows image, it does not need to have pwsh added - only containers that will be used in your actual build pipeline require pwsh.

# Common Known Root Causes from Errors
- `ERROR: Job failed (system failure): unable to upgrade connection: container not found ("build")` 
  - can mean that you are trying to run on a container that does not have `pwsh` installed.
  - can mean that you have a mismatch of major Windows version between the requested container and the Windows node pool - for example executing a 2022 container on a 2019 windows node pool.

# Commands

1. Open Azure CloudShell
2. `script ~\consoletranscript.log #in case CloudShell times out, you will have a log of final results`  
2. `cd clouddrive`
3. `git clone git@gitlab.com:guided-explorations/microsoft/azure-aks/aks-windows.git`
4. `cd aks-windows/gitlab-infra/aks-terraform`
5. `terraform init`
6. `terraform apply`
7. `az aks get-credentials --resource-group gitlab-aks-windows-rg --name glaks1 --file ./azurek8s`
8. `echo 'export KUBECONFIG=./azurek8s' | tee -a ~/.bash_profile` #set var and put in login profile
9. `kubectl describe node akswin000000 | kubectl describe node akswin000000 | grep "windows-build\|\/arch"` #test cluster connection and permissions and win version and hardware arch via k8s labels
10. `kubectl run --rm -it bash -n default --image bash bash  --overrides='{"apiVersion": "v1", "spec": {"nodeSelector": { "kubernetes.io/os": "linux" }}}'`
11. Inside container: `wget -qO- http://ipv4.icanhazip.com` - if you get an IP back, you are wired to the outside world
12. `nano ../gitlab-infra/runner-configs/win-runner.yml` - add runner token
13. `helm install --namespace gitlab-runner --create-namespace win-runner -f ../gitlab-infra/runner-configs/win-runner.yaml gitlab/gitlab-runner --set nodeSelector."kubernetes\\.io/os"=linux`
    **IMPORTANT** The runner targeting the windows node pool runs on linux - do not change this portion of the helm command.
14. Use the following gitlab-ci.yml to test all three flavors of windows: https://gitlab.com/guided-explorations/microsoft/azure-aks/aks-windows/-/blob/master/.gitlab-ci.yml
15. `nano ../gitlab-infra/runner-configs/lin-runner.yml` - add runner token
16. `helm install --namespace gitlab-runner --create-namespace lin-runner -f ../gitlab-infra/runner-configs/lin-runner.yaml gitlab/gitlab-runner --set nodeSelector."kubernetes\\.io/os"=linux`


# Deploy a Webserver Using GitLab Agent

1. New Cluster Management project from template
2. Configure using agent-config/config.yaml
3. Create agent config helm command
4. Webserver installs
5. `kubectl get services -A`
6. Public IP is accessible from the world or within cloud shell with curl.

# Experimentation Tips

`kubectl get pods --all-namespaces --no-headers |  awk '{if ($2 ~ "_YOUR_JOB_POD_PREFACE_*") print $2}' | xargs kubectl -n _YOUR_RUNNER_NAMESPACE_ delete pod` - used to delete multiple job pods when things aren't working out.

## Rough notes:
kubectl create secret docker-registry regcred --docker-server=<your-registry-server> --docker-username=<your-name> --docker-password=<your-pword> --docker-email=<your-email>

# az aks get-credentials --resource-group my-aks-with-nat-aks --name aks-with-nat-gateway --file ./azurek8s

# cat ./azurek8s # If you see << EOT at the beginning and EOT at the end, remove these characters from the file.

## Windows Runner
**IMPORTANT** The runner targeting the windows node pool runs on linux - do not change this portion of the helm command.

[win-runner.yml](gitlab-infra/runner-configs/win-runner.yml)

helm install --namespace gitlab-runner win-runner -f gitlab-infra/runner-configs/win-runner.yaml gitlab/gitlab-runner --set nodeSelector."kubernetes\\.io/os"=linux

## Linux Runner
[lin-runner.yml](gitlab-infra/runner-configs/lin-runner.yml)

helm install --namespace gitlab-runner win-runner -f gitlab-infra/runner-configs/lin-runner.yaml gitlab/gitlab-runner --set nodeSelector."kubernetes\\.io/os"=linux

## GitLab Runner Tokens

This automation can take either the newer [GitLab Runner Authentication Token](https://docs.gitlab.com/ee/security/token_overview.html#runner-authentication-tokens-also-called-runner-tokens) or the depreciated [GitLab Runner Registration Token](https://docs.gitlab.com/ee/security/token_overview.html#runner-registration-tokens-deprecated). It will autodetect whether a given token that starts with 'glrt-' and autoswitch the runner registration/authentication behavior appropriately.

## Debugging and Troubleshooting Instance Runner
[Debugging Guide](./DEBUGGING.md)