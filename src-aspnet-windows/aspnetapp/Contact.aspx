﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="aspnetapp.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3><A HREF="https://about.gitlab.com"><img src="/Content/gitlab-logo-100.png" height="50" width="229"></A></h3>
    <address>
        268 Bush Street #350<br />
        San Francisco, CA 94104-3503<br />
    </address>

    <address>
        <strong>Sales</strong>   <a href="mailto:sales@gitlab.com">sales@gitlab.com</a><br />
    </address>
</asp:Content>
